import matplotlib.pyplot as plt
import cv2
import numpy as np

from PIL import Image

left_img = cv2.imread('data/disp1.png',0)  #read it as a grayscale image
right_img = cv2.imread('data/disp5.png',0)


left_img_color = cv2.imread('data/view1.png')  #read it as a grayscale image
right_img_color = cv2.imread('data/view5.png')



left_np_img = np.asarray(left_img)
right_np_img = np.asarray(right_img)
left_np_img_color = np.asarray(left_img_color)
right_np_img_color = np.asarray(right_img_color)



final_image = np.zeros((left_np_img.shape[0],left_np_img.shape[1],3))
min_SSD = float('inf')
match = 0


for i in range (0,left_np_img.shape[0]-3):
	for j in range (0,left_np_img.shape[1]-3):
		#min_SSD = float('inf')
		for k in range (j,-1,-1):
			sum1 = int(left_np_img[i][j])
			sum2 = int(right_np_img[i][k])
			ssd = (sum1 - sum2)**2;
			if( ssd < min_SSD):
				min_SSD = ssd
				match = k
		
		final_image[i][j][0] = (int(left_np_img_color[i][j][0]) + int(right_np_img_color[i][match][0]))/2
		final_image[i][j][1] = (int(left_np_img_color[i][j][1]) + int(right_np_img_color[i][match][1]))/2
		final_image[i][j][2] = (int(left_np_img_color[i][j][2]) + int(right_np_img_color[i][match][2]))/2		


img = Image.fromarray(final_image, 'RGB')
img.save('view3.png')
img.show()
