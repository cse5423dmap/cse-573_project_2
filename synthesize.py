import Image

background = Image.open("view1.png")
overlay = Image.open("view5.png")

background = background.convert("RGBA")
overlay = overlay.convert("RGBA")

background_pixels = background.load()
overlay_pixels = overlay.load()

for y in xrange(overlay.size[1]):
    for x in xrange(overlay.size[0]):
         background_pixels[x,y] = (background_pixels[x,y][0], background_pixels[x,y][1], background_pixels[x,y][2], 255)

for y in xrange(overlay.size[1]):
    for x in xrange(overlay.size[0]):
         overlay_pixels[x,y] = (overlay_pixels[x,y][0], overlay_pixels[x,y][1], overlay_pixels[x,y][2], 128)

background.paste(overlay)
background.save("new.png","PNG")