import cv2
import numpy as np

left_img = cv2.imread('view1.png', 0)  #read it as a grayscale image
right_img = cv2.imread('view5.png', 0)

#Disparity Computation for Left Image

# Fact: All pixels in left image shift by the disparity value TO THE LEFT in the right image!

# Min_SSD = infinity
# for(i = rows; i < maxrows; i++)
#    for(j = columns; j < maxcolumns; j++)
#       for(k = j; k >= 0; k--) 
#           SSD = Sum_Squared_Diff(left_img(i,j), right_img(i,k)) (either 3x3 or 9x9 block)
#           if( SSD < Min_SSD)
#                Min_SSD = SSD
#                match = k
#       disparity_left(i,j) = j - k









#Disparity Computation for Right Image

# Fact: All pixels in right image shift by the disparity value TO THE RIGHT in the left image!