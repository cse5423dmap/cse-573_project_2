import cv2
import numpy as np
from PIL import Image




def min_of_three(a,b,c):
	min = float('inf')
	if(b > a and b <= c):
		min = a;
	elif(b < c):
		min = b;
	else:
		min = c;
	return min;

def stereo_1d(z):
	v1 = left_img[z]
	v2 = right_img[z]

	v1_len = len(v1)
	v2_len = len(v2)


	d_left = []
	d_right = []

	for i in range(0,v1_len):
		match = 0
		for j in range(0,v2_len):
			if(v1[i] == v2[j]):
				d_left.append(abs(i-j))	
				d_right.append(abs(i-j))
				match = 1
				break
		if(match == 0):
			d_left.append(0)
			d_right.append(0)

	return d_left,d_right

left_img = cv2.imread('Data/view1.png', 0)  #read it as a grayscale image
right_img = cv2.imread('Data/view5.png', 0)

d_left_array = []
d_right_array = []

for i in range(0,left_img.shape[0]):
	a,b = stereo_1d(i)
	d_left_array.append(a)
	d_right_array.append(b)


for i in range(0,len(d_left_array)):
	for j in range(0,len(d_left_array[i])):
		d_left_array[i][j] = int(d_left_array[i][j]/(float(463)/255))
		d_right_array[i][j] = int(d_right_array[i][j]/(float(463)/255))


disparity_left = np.array(d_left_array)
disparity_right = np.array(d_right_array)


img = Image.fromarray(disparity_left, 'RGB')
img.save('disparity_left_2.png')
img.show()

img = Image.fromarray(disparity_right, 'RGB')
img.save('disparity_right_2.png')
img.show()