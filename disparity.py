import matplotlib
import cv2
import numpy as np

left_img = cv2.imread('data/3.png', 0)  #read it as a grayscale image
right_img = cv2.imread('data/4.png', 0)


left_np_img = np.asarray(left_img)
right_np_img = np.asarray(right_img)

'''
left_np_img = np.array([[0,1,2,3],[4,5,6,7],[0,1,2,3],[4,5,6,7],[0,1,2,3]])
#right_np_img = np.array([[7,6,5,4],[3,2,1,0],[7,3,4,6],[5,2,0,1],[0,1,2,3]])
right_np_img = np.array([[0,1,2,3],[4,5,6,7],[7,3,4,6],[5,2,0,1],[0,1,2,3]])
'''
#Disparity Computation for Left Image
print left_np_img
print right_np_img
# Fact: All pixels in left image shift by the disparity value TO THE LEFT in the right image!
disparity_left = np.zeros((left_np_img.shape[0],left_np_img.shape[1]))
min_SSD = float('inf')
match = 0;

s = np.sum((left_np_img.astype("float") - right_np_img.astype("float"))**2)
s /= float(left_np_img.shape[0] * left_np_img.shape[1])
print s;

for i in range (0,left_np_img.shape[0]):
	for j in range (0,left_np_img.shape[1]):
		min_SSD = float('inf')
		for k in range (j,-1,-1):
			ssd = (left_np_img[i,j] - right_np_img[i,k])**2;
			print ('ssd is ' + str(ssd) + '.')
			print ('min ssd is '+str(min_SSD)+ '.')
			if( ssd < min_SSD):
				min_SSD = ssd
				match = k
		disparity_left[i,j] = j - match


print disparity_left

# for(i = rows; i < maxrows; i++)
#    for(j = columns; j < maxcolumns; j++)
#       for(k = j; k >= 0; k--) 
#           SSD = Sum_Squared_Diff(left_img(i,j), right_img(i,k)) (either 3x3 or 9x9 block)
#           if( SSD < Min_SSD)
#                Min_SSD = SSD
#                match = k
#       disparity_left(i,j) = j - match









#Disparity Computation for Right Image

# Fact: All pixels in right image shift by the disparity value TO THE RIGHT in the left image!