import matplotlib
import scipy.misc
import cv2
from matplotlib import pyplot as plt
import numpy as np
from scipy import signal as ss
import time
 
 
def CorrsWithEdgeNC(slit0, slit1, windowSize, lagDis, size):
    '''input two 1D slices from stereo images and correlates
    using block matching with blocks sized windowSize'''
    length = len(slit0) #lag length
    sideBar = (windowSize) / 2 #side bar to skip
 
    lag  = xrange(0-lagDis,lagDis+1)
    lagLength = 2*lagDis+1
 
    slitCorrelations = []
    firstLag = lag[0]
    correl = np.zeros(lagLength)
    for x in xrange(sideBar + (lagLength / 2), length - sideBar - (lagLength / 2), size):
        box0 = slit0[(x - sideBar) : (x + sideBar)]
        for l in lag:
            box1 = slit1[(x - sideBar + l) : (x + sideBar + l)]
 
            sum11 = np.inner(box0,box0) #finding cross correlation
            sum22 = np.inner(box1,box1)
            sum12 = np.inner(box0,box1)
 
            corr = sum12 / (sum11*sum22)**0.5 #make correlation
            correl[l-firstLag] = corr
 
        slitCorrelations.append(0+(np.argmax(correl)-lagDis))
    return(slitCorrelations)
 
#get the images
#img0 = scipy.misc.imread("view1.png", 1) #Change the filename for the images use you use!!!
#img1 = scipy.misc.imread("view5.png", 1)

img0 = cv2.imread('view1.png',0)
img1 = cv2.imread('view3.png',0)

print("Read images...")
 
size = 1
#NOTE: SMALLER NUMBER HERE IS BIGGER SIZE!
#resize the images to make the code faster.
im0_small = scipy.misc.imresize(img0, 0.7, interp='bicubic', mode=None).astype(float)
im1_small = scipy.misc.imresize(img1, 0.7, interp='bicubic', mode=None).astype(float)
 
print("Resized images...")
 
imageLoopData = xrange(0,im0_small.shape[0],size)
 
#Adjust the following numbers, these can change the result.
lag = 10
winsize = 11 #must be odd.
 
#print("Starting Depthmap...")
image = []
t1 = time.clock()
#Repeat the program for each line of the images
for y in imageLoopData:
    #Get a slit from both images
    im0 = im0_small[y] #All of x, 1 pixel high.
    im1 = im1_small[y]
    image.append(CorrsWithEdgeNC(im0, im1, winsize, lag, size)) #Append the row result
t2 = time.clock()-t1
 
med = (ss.medfilt(image,3)+33)*10 #The +33 and *10 are to make the resulting image go across the whole spectrum.
#adjust the second variable in the medfilt to change the smoothness.
print("Done, plotting...")
print("Time took "+str(t2)) #Print the time it took to make the image.
plt.imshow(med,vmin=0,vmax=255) #Show the image
plt.show()