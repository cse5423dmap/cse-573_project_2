import cv2
import numpy as np

def min_of_three(a,b,c):
	min = float('inf')
	if(b > a and b <= c):
		min = a;
	elif(b < c):
		min = b;
	else:
		min = c;
	return min;





left_img = cv2.imread('Data/view1_c.png', 0)  #read it as a grayscale image
right_img = cv2.imread('Data/view1_r.png', 0)

numcols = left_img.shape[1]
numrows = left_img.shape[0]

#Disparity Computation for Left Image

OcclusionCost = 20 #(You can adjust this, depending on how much threshold you want to give for noise)

#For Dynamic Programming you have build a cost matrix. Its dimension will be numcols x numcols

CostMatrix = np.zeros((numcols+1,numcols+1))
DirectionMatrix = np.zeros((numcols+1,numcols+1))  #(This is important in Dynamic Programming. You need to know which direction you need traverse)

#We first populate the first row and column values of Cost Matrix

for i in range(1,numcols+1):
	CostMatrix[i,0] = i*OcclusionCost
	CostMatrix[0,i] = i*OcclusionCost

#THis is where to start 1713 hours

s = left_img[4]
t = right_img[4]

n = len(s)
m = len(t)



D = np.zeros((n+1,m+1))


for i in range(0,n+1):
	D[i,0] = i*OcclusionCost
	D[0,i] = i*OcclusionCost

for i in range(1, m+1):
	for j in range(1, n+1):
		match = D[i-1,j-1] + int(s[j-1]) - int(t[i-1]);
		print "s is "+str(int(s[j-1]))
		print "t is "+str(int(t[i-1]))
		print "match is "+str(match)
		gaps = D[i,j-1] + OcclusionCost;
		print "gaps is "+str(gaps)
		gapt = D[i-1,j] + OcclusionCost;
		print "gapt is "+str(gapt)
		D[i,j] = min_of_three(match,gaps,gapt);

print D


i = m+1
j = n+1
'''
for i in range(1,numcols+1):
	for j in range(1,numcols+1):
		min1 = CostMatrix[i-1,j-1] + abs(int(left_img[i-1,j-1])-int(right_img[i-1,j-1])) 
		#print abs(int(left_img[i-1,j-1])-int(right_img[i-1,j-1])) 
		#print "min1 "+str(min1)
		min2 = CostMatrix[i-1,j] + OcclusionCost
		#print "min2 "+str(min2)
		min3 = CostMatrix[i,j-1] + OcclusionCost
		#print "min3 "+str(min3)
		#print "min is "+ str(min_of_three(min1,min2,min3))
		CostMatrix[i,j] = min_of_three(min1,min2,min3)
		#print "Cij is "+str(CostMatrix[i,j])
		if(min1 == CostMatrix[i,j]):
			DirectionMatrix[i,j] = 1;
		if(min2 == CostMatrix[i,j]):
			DirectionMatrix[i,j] = 2;
		if(min3 == CostMatrix[i,j]):
			DirectionMatrix[i,j] = 3;
'''