import cv2
import numpy as np

left_img = cv2.imread('imagelocation1.jpg', 0)  #read it as a grayscale image
right_img = cv2.imread('imagelocation2.jpg', 0)

#Disparity Computation for Left Image

#OcclusionCost = 20 (You can adjust this, depending on how much threshold you want to give for noise)

#For Dynamic Programming you have build a cost matrix. Its dimension will be numcols x numcols

#CostMatrix = zeros(numcols,numcols)
#DirectionMatrix = zeros(numcols,numcols)  (This is important in Dynamic Programming. You need to know which direction you need traverse)

#We first populate the first row and column values of Cost Matrix

#for(i=0; i < numcols; i++)
#      CostMatrix(i,0) = i*OcclusionCost
#      CostMatrix(0,i) = i*OcclusionCost

# Now, its time to populate the whole Cost Matrix and DirectionMatrix

# Use the pseudocode from "A Maximum likelihood Stereo Algorithm" paper given as reference










