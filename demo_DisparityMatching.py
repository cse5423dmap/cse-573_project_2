import numpy as np
import cv2
from matplotlib import pyplot as plt

imgL = cv2.imread('view1.png',0)
imgR = cv2.imread('view3.png',0)

def mse(imgL, imgR):
	# the 'Mean Squared Error' between the two images is the
	# sum of the squared difference between the two images;
	# NOTE: the two images must have the same dimension
	err = np.sum((imgL.astype("float") - imgR.astype("float")) ** 2)
	err /= float(imgL.shape[0] * imgL.shape[1])
	
	# return the MSE, the lower the error, the more "similar"
	# the two images are
	return err

m = mse(imgL,imgR)

print(m)

plt.show(m)