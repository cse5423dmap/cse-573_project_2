import matplotlib.pyplot as plt
import cv2
import numpy as np

left_img = cv2.imread('data/view1.png')  #read it as a grayscale image
right_img = cv2.imread('data/view3.png')


left_np_img = np.asarray(left_img)
right_np_img = np.asarray(right_img)


#Disparity Computation for Left Image
#print left_np_img
#print right_np_img
# Fact: All pixels in left image shift by the disparity value TO THE LEFT in the right image!
disparity_left = np.zeros((left_np_img.shape[0],left_np_img.shape[1],3))
min_SSD0 = float('inf')
min_SSD1 = float('inf')
min_SSD2 = float('inf')
match0 = 0;
match1 = 0;
match2 = 0;



for i in range (0,left_np_img.shape[0]-3):
	for j in range (0,left_np_img.shape[1]-3):
		#min_SSD = float('inf')
		for k in range (j,-1,-1):
			ssd0 = (int(left_np_img[i][j][0]) - int(right_np_img[i][k][0]))**2;
			ssd1 = (int(left_np_img[i][j][1]) - int(right_np_img[i][k][1]))**2;
			ssd2 = (int(left_np_img[i][j][2]) - int(right_np_img[i][k][2]))**2;
			if( ssd0 < min_SSD0):
				min_SSD0 = ssd0
				match0 = k
		disparity_left[i][j][0] = float(j - match0)
		disparity_left[i][j][1] = float(j - match1)
		disparity_left[i][j][2] = float(j - match2)		



cv2.imshow('disparity_left.png',disparity_left)
cv2.waitKey(0)

disparity_right = np.zeros((right_np_img.shape[0],right_np_img.shape[1],3))
min_SSD0 = float('inf')
min_SSD1 = float('inf')
min_SSD2 = float('inf')
match0 = 0;
match1 = 0;
match2 = 0;

for i in range (0,right_np_img.shape[0]-3):
	for j in range (0,right_np_img.shape[1]-3):
		#min_SSD = float('inf')
		for k in range (j,-1,-1):
			ssd0 = (int(right_np_img[i][j][0]) - int(left_np_img[i][k][0]))**2;
			ssd1 = (int(right_np_img[i][j][1]) - int(left_np_img[i][k][1]))**2;
			ssd2 = (int(right_np_img[i][j][2]) - int(left_np_img[i][k][2]))**2;
			if( ssd0 < min_SSD0):
				min_SSD0 = ssd0
				match0 = k		
		disparity_right[i][j][0] = float(j - match0)
		disparity_right[i][j][1] = float(j - match1)
		disparity_right[i][j][2] = float(j - match2)		



cv2.imshow('disparity_right.png',disparity_right)
cv2.waitKey(0)
