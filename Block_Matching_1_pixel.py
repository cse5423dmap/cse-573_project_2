import matplotlib.pyplot as plt
import cv2
import numpy as np

left_img = cv2.imread('data/view1.png',0)  #read it as a grayscale image
right_img = cv2.imread('data/view5.png',0)


left_np_img = np.asarray(left_img)
right_np_img = np.asarray(right_img)


#Disparity Computation for Left Image
#print left_np_img
#print right_np_img
# Fact: All pixels in left image shift by the disparity value TO THE LEFT in the right image!
disparity_left = np.zeros((left_np_img.shape[0],left_np_img.shape[1]))
min_SSD = float('inf')
match = 0;


for i in range (0,left_np_img.shape[0]-3):
	for j in range (0,left_np_img.shape[1]-3):
		min_SSD = float('inf')
		for k in range (j,-1,-1):
			ssd = (int(left_np_img[i,j]) - int(right_np_img[i,k]))**2;
			if( ssd < min_SSD):
				min_SSD = ssd
				match = k
		disparity_left[i,j] = j - match


print disparity_left

cv2.imshow('disparity_left.png',disparity_left)
cv2.waitKey(0)

disparity_right = np.zeros((left_np_img.shape[0],left_np_img.shape[1]))
min_SSD = float('inf')
match = 0;


for i in range (0,left_np_img.shape[0]-3):
	for j in range (0,left_np_img.shape[1]-3):
		min_SSD = float('inf')
		for k in range (j,-1,-1):
			ssd = ( int(right_np_img[i,j]) - int(left_np_img[i,k]))**2;
			if( ssd < min_SSD):
				min_SSD = ssd
				match = k
		disparity_right[i,j] = j - match


print disparity_right

cv2.imshow('disparity_right.png',disparity_right)
cv2.waitKey(0)
